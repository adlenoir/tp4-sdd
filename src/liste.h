#ifndef LISTE_H
#define LISTE_H

/* ------------------------------------ */
/* 			Structure d'un noeud   		*/
/* ------------------------------------ */
typedef struct cellule
{
    char    cle[40];
    int     valeur;
    struct  cellule *suiv;
} cellule_t, *donnee_t;

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */
cellule_t   *CreerCellule(char *, int);
void        Inserer(donnee_t *, cellule_t *);
void        Supprimer(cellule_t *, cellule_t **);
void        AfficherListe(donnee_t, FILE *);
void        LibererListe(donnee_t);

#endif
