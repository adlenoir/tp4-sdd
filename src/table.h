#ifndef TABLES
#define TABLES

#include "utils.h"
#include "liste.h"

/* ------------------------------------ */
/* 			Structure d'une table       */
/* ------------------------------------ */
typedef struct table
{
    donnee_t majeur[HASH_MAX];
} table_t;

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */
table_t     *CreerTable();
int         Chargement(table_t *, const char *);
donnee_t    Recherche(table_t *, char *);
void        AfficherTable(table_t *, FILE *);
void        LibererTable(table_t *);

#endif
