#ifndef UTILS_H
#define UTILS_H
#define HASH_MAX 29

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */
void            *sddalloc(size_t);
unsigned int    hash_string(const char *str);

#endif