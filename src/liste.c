#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste.h"
#include "utils.h"

/* -------------------------------------------------------------------- */
/* CreerCellule   		    Intialise une cellule                       */
/*                                                                      */
/* Principe:      Malloc puis affecte les valeurs de base               */
/*                                                                      */
/* En entrée: cle = mot, valeur = nb occurence                          */
/*                                                                      */
/* En sortie: Retourne l'adresse de la cellule créée			        */
/*                                                                      */
/* Lexique : tmp : pointeur temporaire sur la nouvelle cellule créée    */
/* -------------------------------------------------------------------- */
cellule_t *CreerCellule(char *cle, int valeur)
{
    cellule_t *tmp = sddalloc(sizeof(cellule_t));

    strcpy(tmp->cle, cle);
    tmp->valeur = valeur;
    tmp->suiv = NULL;

    return tmp;
}

/* -------------------------------------------------------------------- */
/* Inserer          Insère une cellule dans la liste chaînée            */
/*                                                                      */
/* Principe:      Chaîne la cellule en début de liste                   */
/*                                                                      */
/* En entrée: cellule : pointeur sur la cellule que l'on ajoute 		*/
/* 					  dans la liste                         		    */
/*            liste : pointeur de pointeur sur la première cellule de   */
/*                      la sous table                                   */
/* -------------------------------------------------------------------- */
void Inserer(donnee_t *liste, cellule_t *cellule)
{
    cellule->suiv = *liste;
    *liste = cellule;
}

/* -------------------------------------------------------------------- */
/* AfficherListe          Affiche la liste (sous table)                 */
/*                                                                      */
/* Principe:      Parcourt la liste jusqu'à la fin (NULL) et l'affiche  */
/*                                                                      */
/* En entrée: liste : pointeur sur la première cellule                  */
/*            flux : descripteur de fichier pour l'affichage            */
/*                                                                      */
/* Lexique : cour : pointeur courant sur la liste                       */
/* -------------------------------------------------------------------- */
void AfficherListe(donnee_t liste, FILE *flux)
{
    cellule_t *cour = liste;
    while (cour != NULL)
    {
        fprintf(flux, "\t%s %d\n", cour->cle, cour->valeur);
        cour = cour->suiv;
    }
}

/* -------------------------------------------------------------------- */
/* LibererListe          Libère la mémoire allouée pour la liste        */
/*                                                                      */
/* Principe: Parcourt la liste jusqu'à la fin (NULL) et free les cell.  */
/*                                                                      */
/* En entrée: liste : pointeur sur la liste de cellules (sous table)    */
/*                                                                      */
/* Lexique : cour : pointeur courant sur la liste                       */
/*           tmp : pointeur temporaire sur la liste                     */
/* -------------------------------------------------------------------- */
void LibererListe(donnee_t liste)
{
    cellule_t *cour = liste, *tmp;
    while (cour != NULL)
    {
        tmp = cour;
        cour = cour->suiv;
        free(tmp);
    }
}
