#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "utils.h"

/* -------------------------------------------------------------------- */
/* sddalloc          		wrapper de gestion d'erreurs pour malloc    */
/*                                                                      */
/* En entrée: size: taille à allouer								    */
/*                                                                      */
/* En sortie: Un pointeur pointant vers la cellule allouée			    */
/* -------------------------------------------------------------------- */
void *sddalloc(size_t size)
{
    void *alloc = malloc(size);
    if (!alloc)
    {
        fprintf(stderr, "Error while trying malloc !");
        exit(1);
    }
    return alloc;
}

/* -------------------------------------------------------------------- */
/* hash_string          		Crée un hash d'une string 	            */
/*                                                                      */
/* En entrée:	str : chaîne de caractères sur pour générer             */
/*                  la clé de hachage					                */
/*                                                                      */
/* En sortie: Un entier représentant l'indice de la table majeur        */
/* -------------------------------------------------------------------- */
unsigned int hash_string(const char *str)
{
    unsigned int hash = 5381; /*  fonction de hachage de D.J. Bernstein*/
    const char *s;
    for (s = str; *s; s++)
        hash = ((hash << 5) + hash) + tolower(*s);
    return (hash & 0x7FFFFFFF) % HASH_MAX;
}
