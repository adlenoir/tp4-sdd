#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "teZZt.h"

/* -------------------------------------------------------------------- */
/* TESTS        DÉBUT Jeux de tests                                     */
/* -------------------------------------------------------------------- */
BEGIN_TEST_GROUP(TP4)

TEST(TABLE, "Tests basic table")
{
    int i, cpt = 0;

    //test cas de base
    printf("Test du cas de base\n");
    table_t *table = CreerTable();
    Chargement(table, "tests/base.txt");
    AfficherTable(table, stdout);

    CHECK(table->majeur[7]->valeur == 1);
    CHECK(table->majeur[10]->valeur == 2);
    CHECK(table->majeur[14]->valeur == 1);
    CHECK(table->majeur[16]->valeur == 1);
    CHECK(table->majeur[16]->suiv->valeur == 1);
    CHECK(table->majeur[19]->valeur == 1);
    CHECK(table->majeur[24]->valeur == 1);
    CHECK(table->majeur[25]->valeur == 2);

    LibererTable(table);

    //test table vide
    printf("Test de la table vide\n");
    table = CreerTable();
    Chargement(table, "tests/vide.txt");
    AfficherTable(table, stdout);

    for(i=0; i<HASH_MAX; i++){
        if(table->majeur[i] == NULL) cpt++;
    }
    CHECK(cpt == 29);

    LibererTable(table);

    //test avec un seul mot
    printf("Test du cas avec un unique mot\n");
    table = CreerTable();
    Chargement(table, "tests/un_mot.txt");
    AfficherTable(table, stdout);

    CHECK(table->majeur[22]->valeur == 1);

    LibererTable(table);

    //test avec seulement le meme mot
    printf("Test du cas avec un unique mot n fois\n");
    table = CreerTable();
    Chargement(table, "tests/meme_mot.txt");
    AfficherTable(table, stdout);

    CHECK(table->majeur[22]->valeur == 6);

    LibererTable(table);

    //test avec plusieurs phrases
    printf("Test du cas avec plusieurs phrases\n");
    table = CreerTable();
    Chargement(table, "tests/phrases.txt");
    AfficherTable(table, stdout);

    CHECK(table->majeur[22]->valeur == 6);

    LibererTable(table);
}

END_TEST_GROUP(TP4)

int main() 
{
    RUN_TEST_GROUP(TP4);
    return EXIT_SUCCESS;
}