#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"

/* -------------------------------------------------------------------- */
/* CreerTable          		Creer une table vide 	                    */
/*                                                                      */
/* Principe:      Malloc puis affecte les valeurs de base               */
/*                                                                      */
/* En sortie: L'adresse d'une table correctement initialisée		    */
/*                                                                      */
/* Lexique : i : variable d'itération                                   */
/*           table : pointeur sur la table                              */
/* -------------------------------------------------------------------- */
table_t *CreerTable()
{
    int i;
    table_t *table = sddalloc(sizeof(table_t));

    for (i = 0; i < HASH_MAX; ++i)
    {
        table->majeur[i] = NULL;
    }

    return table;
}

/* -------------------------------------------------------------------- */
/* Chargement   Lis et ajoute les données d'un fichier dans la table    */
/*                                                                      */
/* Principe:      Ouvre, parcourt le fichier, regarde si le mot         */
/*                existe ou non et crée une cellule ou incrémente       */
/*                                                                      */
/* En entrée: table : pointeur sur la table							    */
/* 			  fichier : nom du fichier à lire                           */
/*                                                                      */
/* En sortie: Code de retour de la fonction       						*/
/*                                                                      */
/* Lexique : flot : pointeur de fichier                                 */
/*           code : code de retour de la fonction                       */
/*           mot : chaîne de caractères pour la lecture des mots        */
/*           tmp : pointeur temporaire sur la nouvelle cellule          */
/* -------------------------------------------------------------------- */
int Chargement(table_t *table, const char *fichier)
{
    int code;
    cellule_t *tmp;
    char mot[40];
    FILE *flot = fopen(fichier, "r");

    if (!flot)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier.\n");
        code = EXIT_FAILURE;
    }
    else
    {
        fscanf(flot, "%s", mot);

        while (!feof(flot))
        {
            mot[strcspn(mot, ".")] = 0;
            tmp = Recherche(table, mot);

            if (tmp == NULL)
            {
                tmp = CreerCellule(mot, 1);
                    Inserer(&(table->majeur[hash_string(tmp->cle)]), tmp);
            }
            else 
            {
                ++tmp->valeur;
            }

            fscanf(flot, "%s", mot);
        }

        code = EXIT_SUCCESS;
        fclose(flot);
    }

    return code;
}

/* -------------------------------------------------------------------- */
/* Recherche   Recherche la valeur pour une clé donnée, dans la table   */
/*                                                                      */
/* Principe:      Parcourt chaque sous-table jusqu'à la fin (NULL) et   */
/*                s'arrête si il trouve une clé égale                   */
/*                                                                      */
/* En entrée: table : pointeur sur la table 						    */
/* 			  cle : clé à rechercher                                    */
/*                                                                      */
/* En sortie: Adresse de la cellule trouvé sinon NULL					*/
/*                                                                      */
/* Lexique : cour : pointeur courant sur une cellule                    */
/* -------------------------------------------------------------------- */

donnee_t Recherche(table_t *table, char *cle)
{
    donnee_t cour = table->majeur[hash_string(cle)];

    while (cour != NULL && strcmp(cour->cle, cle) != 0)
    {
        cour = cour->suiv;
    }

    return cour;
}

/* -------------------------------------------------------------------- */
/* AfficherTable          Affiche la table                              */
/*                                                                      */
/* Principe:      Parcourt la table jusqu'à la fin (NULL) et l'affiche  */
/*                                                                      */
/* En entrée: table : pointeur sur la table à afficher                  */
/*            flux : descripteur de fichier pour l'affichage            */
/*                                                                      */
/* Lexique : i : variable d'itération                                   */
/* -------------------------------------------------------------------- */
void AfficherTable(table_t *table, FILE *flux)
{
    int i;
    puts("\n--------------------- DEBUT --------------------------");
    for (i = 0; i < HASH_MAX; ++i)
    {
        fprintf(flux, "Sous-table %d\n", i);
        AfficherListe(table->majeur[i], flux);
    }
    puts("--------------------- FIN --------------------------\n");
}

/* -------------------------------------------------------------------- */
/* LibererTable      Libère la mémoire allouée pour la table            */
/*                                                                      */
/* Principe: Parcourt la table jusqu'à la fin (NULL) et                 */
/*           free chaque sous-table.                                    */
/*                                                                      */
/* En entrée: table : pointeur sur la table                             */
/*                                                                      */
/* Lexique : i : variable d'itération                                   */
/* -------------------------------------------------------------------- */
void LibererTable(table_t *table)
{
    int i;
    if (table != NULL)
    {
        for (i = 0; i < HASH_MAX; ++i)
        {
            LibererListe(table->majeur[i]);
        }
    }
    free(table);
}
